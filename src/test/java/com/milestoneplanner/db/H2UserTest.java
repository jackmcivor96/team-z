package com.milestoneplanner.db;

import com.milestoneplanner.models.Milestone;
import com.milestoneplanner.util.Password;
import org.eclipse.jetty.server.Authentication;
import org.h2.engine.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class H2UserTest {

    private H2User db;

    @Before
    public void setUp() {
        db = new H2User(new ConnectionSupplier(ConnectionSupplier.MEMORY));
    }

    @After
    public void tearDown() {
        try {
            db.close();
        } catch (SQLException e) {
            fail();
        }
    }

    @Test
    public void register() {
        db.register("name","pass");
        assertTrue(db.isRegistered("name"));
    }

    @Test
    public void isRegistered() {
        db.register("name","pass");
        assertTrue(db.isRegistered("name"));
    }

    @Test
    public void isRegisteredFail() {
        db.register("name","pass");
        assertFalse(db.isRegistered("notname"));
    }

    @Test
    public void testPassword() {
        String hash = Password.createHash("Password HeRe");
        assertTrue(Password.validatePassword("Password HeRe", hash));
    }

    @Test
    public void testPasswordFail() {
        String hash = Password.createHash("Password Here");
        assertFalse(Password.validatePassword("not thE same", hash));
    }
}