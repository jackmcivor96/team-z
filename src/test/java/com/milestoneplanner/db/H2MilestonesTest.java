package com.milestoneplanner.db;

import com.milestoneplanner.models.Milestone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class H2MilestonesTest {

    private H2Milestones db;

    @Before
    public void setUp() throws Exception {

        db = new H2Milestones(new ConnectionSupplier(ConnectionSupplier.MEMORY));
    }

    @After
    public void tearDown() throws Exception {
        try {
            db.close();
        } catch (SQLException e) {
            fail();
        }
    }

    @Test
    public void getById() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "");
        db.add("Find Tiger", "Look for the tiger", "17/03/11", "02/05/12", "paul", "");

        Milestone user = db.get(2);
        assertEquals("Look for the dog", user.getDescription());
    }

    @Test
    public void addCompDate() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "");

        db.addCompDate(1, "01/01/01");
        Milestone user = db.get(1);
        assertEquals("01/01/01", user.getCompletionDate());
    }

    @Test
    public void addMilestone() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "");

        List<Milestone> out = db.list();
        assertEquals(2, out.size());
    }

    @Test
    public void listMilestones() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "");

        List<Milestone> out = db.list();
        assertEquals(2, out.size());
    }

    @Test
    public void getListShareCode() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "12345");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "12345");
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "22222");
        db.add("Find Snake", "Look for the snake", "01/02/11", "02/05/12", "john", "12345");
        db.add("Find Bat", "Look for the bat", "08/07/11", "15/04/12", "paul", "22222");

        List<Milestone> out = db.getListShareCode("12345");
        assertEquals(3, out.size());
    }

    @Test
    public void selectMilestoneByUser() {
        db.add("Find Penguin", "Look for the penguin", "01/04/11", "02/07/12", "john", "");
        db.add("Find Dog", "Look for the dog", "08/02/11", "09/04/12", "john", "");
        db.add("Find Snake", "Look for the snake", "01/02/11", "02/05/12", "john", "");
        db.add("Find Bat", "Look for the bat", "08/07/11", "15/04/12", "paul", "");

        List<Milestone> out = db.user("john");
        assertEquals(3, out.size());
    }

    @Test
    public void deleteMilestone() {
        db.add("Find Bear", "Look for bear", "01/01/12", "02/02/12", "tom", "");
        db.add("Find Lion", "Look for the lion", "18/04/12", "22/04/12", "tom", "");
        db.add("Find Snake", "Look for the snake", "08/02/11", "02/04/12", "john", "");

       db.delete(2);
       List<Milestone> out = db.list();
       assertEquals(2, out.size());
    }
}