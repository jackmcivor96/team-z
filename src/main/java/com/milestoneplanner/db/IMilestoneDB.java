package com.milestoneplanner.db;

import com.milestoneplanner.models.Milestone;

import java.util.List;

public interface IMilestoneDB {
    /**
     * Get milestone with given id
     * @param id the id to look up
     * @return the milestone or null if there is none
     */
    Milestone get(long id);

    /**
     * List all milestones
     * @return A list of all messages, or an empty list if there are none
     */
    List<Milestone> list();

    List<Milestone> getListShareCode(String shareCode);

    /**
     * Add a milestone from a given user.  The timestamp will be <code>new Date().getTime()</code>
     * @param name  name text
     * @param description  description text
     * @param dueDate  duedate text
     * @param completionDate  completiondate text
     * @param user User name
     * @param shareCode Share Code
     */
    void add(String name, String description, String dueDate, String completionDate, String user, String shareCode);

    /**
     * List messages for a given user
     * @param user  User name
     * @return The milestone from this user, or the empty list if none
     */
    List<Milestone> user(String user);

    /**
     * Delete milestone with the given id, if it exists
     * @param id The id
     */
    void delete(long id);

    /**
     * Update milestone with the given id, if it exists
     * @param id The id
     */

    /**
     * Update completion date of milestone with the given id, if it exists
     * @param id The id
     */
    void addCompDate(long id, String completionDate);



}
