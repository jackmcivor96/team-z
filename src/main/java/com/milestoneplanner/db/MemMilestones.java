package com.milestoneplanner.db;

import com.milestoneplanner.models.Milestone;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MemMilestones implements IMilestoneDB {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(MemMilestones.class);

    private final List<Milestone> milestones;
    private long index;

    public MemMilestones() {
        milestones = new ArrayList<>();
        index = 0;
    }

    @Override
    public synchronized Milestone get(long id) {
        for (Milestone m: milestones) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }

    @Override
    public synchronized List<Milestone> list() {
        return Collections.unmodifiableList(milestones);
    }

    @Override
    public synchronized void add(@NonNull String name, @NonNull String description, String dueDate, String completionDate, String user, String shareCode) {
        Milestone m = new Milestone(index++, name, description, dueDate, completionDate, user, shareCode);
        milestones.add(m);
    }

    @Override
    public synchronized List<Milestone> user(@NonNull String user) {
        List<Milestone> out = milestones.stream().filter(m -> user.equals(m.getUser())).collect(Collectors.toList());
        return Collections.unmodifiableList(out);
    }

    public synchronized List<Milestone> getListShareCode(String shareCode) {
        List<Milestone> out = milestones.stream().filter(m -> shareCode.equals(m.getShareCode())).collect(Collectors.toList());
        return Collections.unmodifiableList(out);
    }

    @Override
    public synchronized void delete(long id) {
        for (Milestone m: milestones) {
            if (id == m.getId()) {
                milestones.remove(m);
                return;
            }
        }
    }


    @Override
    public synchronized void addCompDate(long id, String completionDate) {
        for (Milestone m: milestones) {
            if (id == m.getId()) {
                String updateCompletionDate = completionDate;
                m.setCompletionDate(updateCompletionDate);
                return;
            }
        }
    }


}