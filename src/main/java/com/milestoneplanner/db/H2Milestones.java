package com.milestoneplanner.db;
import com.milestoneplanner.models.Milestone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class H2Milestones extends H2Base implements IMilestoneDB {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(H2Milestones.class);


    public H2Milestones(ConnectionSupplier connectionSupplier) {
        super(connectionSupplier.provide());
        try {
            initTable(getConnection());
        } catch (Exception e) {
            LOG.error("Can't find database driver: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void initTable(Connection conn) throws SQLException {
        execute(conn, "CREATE TABLE IF NOT EXISTS " +
                "milestones (id BIGINT AUTO_INCREMENT, name VARCHAR(30), description VARCHAR(100), dueDate  VARCHAR(15), completionDate  VARCHAR(15), user VARCHAR(255), shareCode VARCHAR(255), PRIMARY KEY(id))");
    }

    @Override
    public Milestone get(long id) {
        String ps = "SELECT id, name, description, duedate, completiondate, user FROM milestones where id = ?";
        Connection conn = getConnection();
        try {
            Milestone out = null;
            PreparedStatement p = conn.prepareStatement(ps);
            p.setLong(1, id);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Milestone m = rs2milestone(rs);
                out = m;
            }
            return out;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public List<Milestone> user(String user) {
        String ps = "SELECT id, name, description, dueDate, completionDate, user, shareCode FROM milestones WHERE user = ?";
        Connection conn = getConnection();
        try {
            List<Milestone> out = new ArrayList<>();
            PreparedStatement p = conn.prepareStatement(ps);
            p.setString(1, user);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Milestone m = rs2milestone(rs);
                out.add(m);
            }
            return out;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public List<Milestone> list() {
        String ps = "SELECT id, name, description, dueDate, completionDate, user, shareCode FROM milestones";
        Connection conn = getConnection();
        try {
            List<Milestone> out = new ArrayList<>();
            PreparedStatement p = conn.prepareStatement(ps);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Milestone m = rs2milestone(rs);
                out.add(m);
            }
            return out;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public List<Milestone> getListShareCode(String shareCode) {
        String ps = "SELECT id, name, description, dueDate, completionDate, user, shareCode FROM milestones WHERE shareCode = ?";
        Connection conn = getConnection();
        try {
            List<Milestone> out = new ArrayList<>();
            PreparedStatement p = conn.prepareStatement(ps);
            p.setString(1, shareCode);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Milestone m = rs2milestone(rs);
                out.add(m);
            }
            return out;
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public void add(String name, String description, String dueDate, String completionDate, String user, String shareCode) {
        String ps = "INSERT INTO milestones (name, description, dueDate, completionDate, user, shareCode) VALUES(?,?,?,?,?,?)";
        Connection conn = getConnection();
        try (PreparedStatement p = conn.prepareStatement(ps)) {
            p.setString(1, name);
            p.setString(2, description);
            p.setString(3, dueDate);
            p.setString(4, completionDate);
            p.setString(5, user);
            p.setString(6, shareCode);
            p.execute();
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public void delete(long id) {
        Connection conn = getConnection();
        String ps = "DELETE FROM milestones WHERE id = ?";
        try (PreparedStatement p = conn.prepareStatement(ps)) {
            p.setLong(1, id);
            p.execute();
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    @Override
    public void addCompDate(long id, String completionDate) {
        Connection conn = getConnection();
        String ps = "UPDATE milestones SET completionDate = ? WHERE id = ?";
        try (PreparedStatement p = conn.prepareStatement(ps)) {
            p.setString(1, completionDate);
            p.setLong(2, id);
            p.execute();
        } catch (SQLException e) {
            throw new H2MilestonesException(e);
        }
    }

    private static Milestone rs2milestone(ResultSet rs) throws SQLException {
        return new Milestone(rs.getLong(1), rs.getString(2),
                rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(2), rs.getString(6));
    }

    public static final class H2MilestonesException extends RuntimeException {
        H2MilestonesException(Exception e) {
            super(e);
        }
    }
}
