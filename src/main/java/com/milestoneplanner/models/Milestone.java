package com.milestoneplanner.models;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Milestone {

    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(Milestone.class);

    private long id;
    private String name;
    private String description;
    private String dueDate;
    private String completionDate;
    private String user;
    private String shareCode;

    public Milestone(long id, String name, String description, String dueDate, String completionDate, String user, String shareCode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.completionDate = completionDate;
        this.user = user;
        this.shareCode = shareCode;
    }
    // dates stored Strings just now dueDate, completionDate

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getShareCode() { return shareCode; }

    public void setShareCode(String shareCode) { this.shareCode = shareCode; }
}
