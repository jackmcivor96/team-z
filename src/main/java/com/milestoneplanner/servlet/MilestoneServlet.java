package com.milestoneplanner.servlet;

import com.google.common.base.Charsets;
import com.milestoneplanner.db.H2User;
import com.milestoneplanner.db.IMilestoneDB;
import com.milestoneplanner.models.Milestone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class MilestoneServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(MilestoneServlet.class);

    private static final String MILESTONES_TEMPLATE = "milestones.mustache";

    private static final String NAME_PARAMETER = "name";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String DUEDATE_PARAMETER = "dueDate";
    private static final String COMPLETIONDATE_PARAMETER = "completionDate";
    private static final String SHARE_PARAMETER = "shareCode";
    private static final String METHOD_PARAMETER = "method";
    private static final String ID_PARAMETER = "msgId";

    private final IMilestoneDB db;
    private final H2User userDB;

    @Inject
    public MilestoneServlet(IMilestoneDB db, H2User userDB) {
        this.db = db;
        this.userDB = userDB;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String userName = UserFuncs.getCurrentUser(request);
        String loggedInUser = UserFuncs.getCurrentUser(request);

        if (!userDB.isRegistered(userName)) {
            String err = "can't find user " + userName;
            issue("text/plain", HttpServletResponse.SC_NOT_FOUND, err.getBytes(Charsets.UTF_8), response);
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("user", userName);
        map.put("matches", loggedInUser.equals(userName));

        List<Milestone> milestones = db.user(userName);
        if (milestones.size() > 0) {
            map.put("milestones", milestones);
        }
        showView(response, MILESTONES_TEMPLATE, map);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String method = getString(request, METHOD_PARAMETER, "post");
        if ("Add".equals(method)) {
            doAdd(request, response);
        }  else if ("Update".equals(method)) {
            doUpdate(request, response);
        }  else if ("Share".equals(method)) {
            addShareCode(request, response);
        }  else if ("Delete".equals(method)) {
            doDelete(request, response);
        } else if ("Create".equals(method)) {

            String name = request.getParameter(NAME_PARAMETER);
            String description = request.getParameter(DESCRIPTION_PARAMETER);
            String dueDate = request.getParameter(DUEDATE_PARAMETER);
            String completionDate = request.getParameter(COMPLETIONDATE_PARAMETER);
            String user = UserFuncs.getCurrentUser(request);
            String shareCode = request.getParameter(SHARE_PARAMETER);
            db.add(name, description, dueDate, completionDate, user, shareCode);
            response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));

        } else {
            String userName = userFromRequest(request);

            if (!userDB.isRegistered(userName)) {
                String err = "can't find user " + userName;
                issue("text/plain", HttpServletResponse.SC_NOT_FOUND, err.getBytes(Charsets.UTF_8), response);
                return;
            }
            response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
        }
    }

    // Add completion date
    protected void doAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String loggedInUser = UserFuncs.getCurrentUser(request);
        long id = getLong(request, ID_PARAMETER);
        Milestone m = db.get(id);

        if (m != null && loggedInUser.equals(m.getUser())) {
            Milestone mil = db.get(id);
            String completionDate = request.getParameter(COMPLETIONDATE_PARAMETER);
            mil.setCompletionDate(completionDate);
        }
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    // Add share code to all milestones
    protected void addShareCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String loggedInUser = UserFuncs.getCurrentUser(request);

        List<Milestone> mList = db.user(loggedInUser);
        List<Milestone> mListAll = db.list();

        Random rand = new Random();
        int  n = rand.nextInt(40000) + 1;

        // Check number not in use
        for (Milestone m : mListAll) {
            if  (String.valueOf(n) == m.getShareCode()) {
                n = rand.nextInt(40000) + 1;
            }
        }
        // set shareCode to random number
        for (Milestone temp : mList) {
            temp.setShareCode(String.valueOf(n));
        }
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }


    protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String loggedInUser = UserFuncs.getCurrentUser(request);
        long id = getLong(request, ID_PARAMETER);
        Milestone m = db.get(id);

        if (m != null && loggedInUser.equals(m.getUser())) {
            Milestone mil = db.get(id);

            String name;
            String description;
            String dueDate;

            if (!request.getParameter(NAME_PARAMETER).equals("") ) {
                name = request.getParameter(NAME_PARAMETER);
                mil.setName(name);
            }
            if (!request.getParameter(DESCRIPTION_PARAMETER).equals("")) {
                description = request.getParameter(DESCRIPTION_PARAMETER);
                mil.setDescription(description);
            }
            if (!request.getParameter(DUEDATE_PARAMETER).equals("")) {
                dueDate = request.getParameter(DUEDATE_PARAMETER);
                mil.setDueDate(dueDate);
            }
        }
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String loggedInUser = UserFuncs.getCurrentUser(request);
        long id = getLong(request, ID_PARAMETER);

        Milestone m = db.get(id);
        if (m != null && loggedInUser.equals(m.getUser())) {
            db.delete(id);
        }
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    static String userFromRequest(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String[] sub = uri.split("/");
        if (sub.length == 3) {
            return sub[2];
        }
        return "";
    }


}
