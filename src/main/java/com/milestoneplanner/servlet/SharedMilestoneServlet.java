package com.milestoneplanner.servlet;

import com.google.common.base.Charsets;
import com.milestoneplanner.db.H2User;
import com.milestoneplanner.db.IMilestoneDB;
import com.milestoneplanner.models.Milestone;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.sql.Types.NULL;


public class SharedMilestoneServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(SharedMilestoneServlet.class);

    private static final String SHARED_MILESTONES_TEMPLATE = "sharedMilestone.mustache";

    private static final String NAME_PARAMETER = "name";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String DUEDATE_PARAMETER = "dueDate";
    private static final String COMPLETIONDATE_PARAMETER = "completionDate";
    private static final String METHOD_PARAMETER = "method";
    private static final String ID_PARAMETER = "msgId";

    private final IMilestoneDB db;
    private final H2User userDB;

    @Inject
    public SharedMilestoneServlet(IMilestoneDB db, H2User userDB) {
        this.db = db;
        this.userDB = userDB;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }

        Map<String, Object> map = new HashMap<>();

        String param = request.getParameter("code");

        if (param.equals(NULL)) {
            String err = "NULL ";
            issue("text/plain", HttpServletResponse.SC_NOT_FOUND, err.getBytes(Charsets.UTF_8), response);
            return;
        }

        List<Milestone> shareList = db.getListShareCode(param);
        if (shareList.size() > 0) {
            map.put("milestones", shareList);
        }
        showView(response, SHARED_MILESTONES_TEMPLATE, map);
    }
}
