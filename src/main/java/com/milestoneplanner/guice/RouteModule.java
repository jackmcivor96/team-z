package com.milestoneplanner.guice;

import com.google.inject.servlet.ServletModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.milestoneplanner.servlet.*;

class RouteModule extends ServletModule {
    @SuppressWarnings({"unused"})
    static final Logger LOG = LoggerFactory.getLogger(RouteModule.class.getName());


    RouteModule() {}

    @Override
    protected void configureServlets() {
        serve("/").with(LoginServlet.class);
        serve("/login").with(LoginServlet.class);
        serve("/register").with(RegisterServlet.class);
        serve("/logout").with(LogoutServlet.class);
        serve("/milestones").with(MilestoneServlet.class);

        serve("/sharedMilestone/").with(SharedMilestoneServlet.class);



    }
}
