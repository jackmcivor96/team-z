package com.milestoneplanner.guice;


import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.milestoneplanner.db.IMilestoneDB;
import com.milestoneplanner.db.MemMilestones;
import com.milestoneplanner.servlet.SharedMilestoneServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.milestoneplanner.servlet.LogoutServlet;
import com.milestoneplanner.servlet.MilestoneServlet;
import com.milestoneplanner.db.ConnectionSupplier;
import com.milestoneplanner.db.H2User;


class BindingModule extends AbstractModule {
    @SuppressWarnings({"unused"})
    static final Logger LOG = LoggerFactory.getLogger(BindingModule.class);

    BindingModule() {}

    @Override
    protected void configure() {

        bind(MilestoneServlet.class).in(Scopes.SINGLETON);
        bind(LogoutServlet.class).in(Scopes.SINGLETON);
        bind(SharedMilestoneServlet.class).in(Scopes.SINGLETON);

        bind(H2User.class).toInstance(new H2User(new ConnectionSupplier(ConnectionSupplier.MEMORY)));
        bind(IMilestoneDB.class).toInstance(new MemMilestones());
    }
}
