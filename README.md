# Milestone Planner #

A milestone planner where user can sign up or log in and then create, edit, update, delete and share there milesstones.

### To run ###

Load project into intelij and run webrunner.

### Built With ###

* Java
* Maven
* Mustache
* jquery
* Bootstrap

### Author ###

* Martin Brown
